#!/bin/bash

PKGBUILD_NAME=${PKGBUILD_NAME:-'PKGBUILD'}
pacman --noconfirm -Syu --needed base-devel gnupg

# sudo lacks suid bit in recent docker images, it seems?
# hopefully temp workaround:
chmod 4755 /usr/bin/sudo

# NOTE: temporary workaround on aarch64 to manually add the wasi any packages
# while they are unavailable for aarch64/ALARM

if [[ ${CARCH} = "aarch64"  ]]; then
  pacman --noconfirm --needed -S wget
  wget https://archlinux.org/packages/community/any/wasi-compiler-rt/download -O wasi-compiler-rt-15.0.7-1-any.pkg.tar.zst
  wget https://archlinux.org/packages/community/any/wasi-libc/download -O wasi-libc-1_0+314+a1c7c2c-1-any.pkg.tar.zst
  wget https://archlinux.org/packages/community/any/wasi-libc++/download -O wasi-libc++-15.0.7-2-any.pkg.tar.zst
  wget https://archlinux.org/packages/community/any/wasi-libc++abi/download -O wasi-libc++abi-15.0.7-2-any.pkg.tar.zst

  pacman --noconfirm -U wasi-compiler-rt-15.0.7-1-any.pkg.tar.zst wasi-libc-1_0+314+a1c7c2c-1-any.pkg.tar.zst wasi-libc++-15.0.7-2-any.pkg.tar.zst wasi-libc++abi-15.0.7-2-any.pkg.tar.zst
fi

# this is a very ugly fix for recent makepkg-5.1-chmod-shenanigans, which mess up the build process in docker
sed -E -i 's/^chmod a-s \"\$BUILDDIR\"$/# chmod a-s \"\$BUILDDIR\"/' `which makepkg`
echo 'nobody ALL=(ALL) NOPASSWD: /usr/bin/pacman' >> /etc/sudoers
mkdir -p /home/nobody && chown -R nobody /home/nobody
usermod -d /home/nobody nobody
# we need to un-expire the account, otherwise PAM will complain
usermod -e '' nobody
chown -R nobody .

if [[ -n "${AARCH64_NO_PGO}"  ]]; then
  sed -i 's/_build_profiled_aarch64=true/_build_profiled_aarch64=false/' "${PKGBUILD_NAME}"
fi
if [[ -n "${X86_64_NO_PGO}"  ]]; then
  sed -i 's/_build_profiled_x86_64=true/_build_profiled_x86_64=false/' "${PKGBUILD_NAME}"
fi

sudo -u nobody -E -H gpg --import KEY
# makepkg will not run as root
sudo -u nobody -E -H makepkg --noconfirm --nosign --syncdeps --cleanbuild -p "${PKGBUILD_NAME}"
# if [[ ! -z "${GLOBAL_MENUBAR}" ]];then
  # mv  "librewolf-${pkgver}-${pkgrel}-${CARCH}.pkg.tar.zst" "librewolf-${pkgver}-${pkgrel}-${CARCH}.global_menubar.pkg.tar.zst"
# fi
